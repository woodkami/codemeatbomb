﻿using UnityEngine;
using System.Collections;

public class EMPwave : MonoBehaviour {
	private float timer;
	public int level;
	// Use this for initialization
	void Start () {
		timer = 2.0f * level;
	}

	// Update is called once per frame
	void OnTriggerEnter(Collider other){
		if (timer > 0)
		other.gameObject.GetComponent<Enemy> ().debuff = 1.0f - 0.2f*level;
		if (timer <= 0)
		other.gameObject.GetComponent<Enemy> ().debuff = 1.0f;
	}
	void OnTriggerStay(Collider other){
		if (timer > 0)
			other.gameObject.GetComponent<Enemy> ().debuff = 1.0f - 0.2f*level;
		if (timer <= 0) {
		other.gameObject.GetComponent<Enemy> ().debuff = 1.0f;
			Destroy(gameObject);
		}
		timer -= Time.deltaTime;
	}
}
