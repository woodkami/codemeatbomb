﻿using UnityEngine;
using System.Collections;

public class GameStart : MonoBehaviour 
{
    // Start Screen

    void OnGUI()
    {
        if (GUI.Button(new Rect(30.0f, 30.0f, 150.0f, 30.0f), "Start Game"))
        {
            StartGame();
        }
    }

    private void StartGame()
    {
        print("Starting Game");

        DontDestroyOnLoad(GameState.Instance);
        GameState.Instance.StartState();
    }
}
