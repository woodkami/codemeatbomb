﻿using UnityEngine;
using System.Collections;

public class ChangeColor : MonoBehaviour {

	// Use this for initializatio
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.R))
		{
			gameObject.renderer.material.color = Color.black;
		}
		if(Input.GetKeyDown(KeyCode.G))
		{
			gameObject.renderer.material.color = Color.red;
		}
		if(Input.GetKeyDown(KeyCode.B))
		{
			gameObject.renderer.material.color = Color.gray;
		}
	}
}
