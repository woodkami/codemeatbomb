﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	private GameObject target;
	public float speed;
	public float debuff;
	public int life;
	// Use this for initialization
	void Start(){
		target = GameObject.FindGameObjectWithTag("tower");
		debuff = 1.0f;
	}

	
	// Update is called once per frame
	void Update () {
		if (life >= 0) {
						if (gameObject.activeSelf)
								transform.position = Vector3.Lerp (transform.position, target.transform.position, speed * debuff * Time.deltaTime);
				} else
						Destroy (gameObject);
	}
}
