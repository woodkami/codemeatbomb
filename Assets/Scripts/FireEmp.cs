﻿using UnityEngine;
using System.Collections;

public class FireEmp : MonoBehaviour {
		public Rigidbody empPrefab;
		private Vector3 target;
		
		public void OnMouseDown(){
			target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Rigidbody bulletInstance;
			bulletInstance = Instantiate (empPrefab,transform.position,transform.rotation) as Rigidbody;
			bulletInstance.gameObject.GetComponent<EMP>().SetTarget (target);
		}
	}