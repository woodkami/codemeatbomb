using UnityEngine;
using System.Collections;

public class TowerDet : MonoBehaviour {
	public GameObject shooter;
	private int enemyNumber;
	public string target = "enemy" ;
	public float timer = 1 ;



	void Start () {

	}

	void OnTriggerEnter (Collider other){
		if (other.gameObject.tag == target) {
			shooter.GetComponent<Fire> ().fire (other.gameObject.transform);
			Debug.Log("Detect done");
				}
	}

	void OnTriggerStay (Collider other){
		if (other.gameObject.tag == target) {
			if(timer < 0){
			shooter.GetComponent<Fire> ().fire (other.gameObject.transform);
			Debug.Log("Detect done");
				timer = 1;
			}
			timer -= Time.deltaTime;
		}

	}
}
