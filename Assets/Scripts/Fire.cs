﻿using UnityEngine;
using System.Collections;

public class Fire : MonoBehaviour
{
	public Rigidbody bulletPrefab;

	public void fire(Transform target){
				Rigidbody bulletInstance;
				bulletInstance = Instantiate (bulletPrefab,transform.position,transform.rotation) as Rigidbody;
				bulletInstance.gameObject.GetComponent<BulletDet>().SetTarget (target);
		}
}