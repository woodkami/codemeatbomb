﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour 
{
    public Transform target;
    public GameObject enemy;

    public float spawnTime = 0.5f;
    float spawnTimeLeft = 0.5f;

    void Update()
    {
        if (spawnTimeLeft <= 0)
        {
            GameObject go = (GameObject)Instantiate(enemy, transform.position, transform.rotation);
            go.GetComponent<EnemyAI>().target = target;
            spawnTimeLeft = spawnTime;
        }
        else
        {
            spawnTimeLeft -= Time.deltaTime;
        }
    }
}
