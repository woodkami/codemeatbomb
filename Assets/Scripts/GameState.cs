﻿using UnityEngine;
using System.Collections;

public class GameState : MonoBehaviour 
{
    private static GameState instance;

    private string activeLevel;
    private int exp;

    public static GameState Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameObject("gamestate").AddComponent<GameState>();
            }

            return instance;
        }
    }

    public void OnApplicationQuit()
    {
        instance = null;
    }

    public void StartState()
    {
        print("Creating a new game state");

        activeLevel = "Level 1";
        exp = 100;

        Application.LoadLevel("level1");
    }

    // Setters & Getters

    public string getLevel() { return activeLevel; }
    public void setLevel(string newLevel) { activeLevel = newLevel; }

    public int getExp() { return this.exp; }
    public void setExp(int exp) { exp = this.exp; }
}
