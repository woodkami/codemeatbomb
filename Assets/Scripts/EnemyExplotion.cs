﻿using UnityEngine;
using System.Collections;

public class EnemyExplotion : MonoBehaviour 
{
    public float duration = 1.0f;

	// Use this for initialization
	void Start () 
    {

	}
	
	// Update is called once per frame
	void Update () 
    {
        if (duration <= 0)
        {
            Destroy(gameObject);
        }
        else
        {
            duration -= Time.deltaTime;
        }
	}
}
