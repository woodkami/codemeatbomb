﻿using UnityEngine;
using System.Collections;

public class LevelGUI : MonoBehaviour
{
    // Initialize level
    void Start()
    {
        print("Loaded: " + GameState.Instance.getLevel());
    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(30, 30, 150, 30), "Load Level 1"))
        {
            GameState.Instance.setLevel("Level 1");
            Application.LoadLevel("level1");
        }

        if (GUI.Button(new Rect(300, 30, 150, 30), "Load Level 2"))
        {
            print("Moving to level 2");
            GameState.Instance.setLevel("Level 2");
            Application.LoadLevel("level2");
        }

        GUI.Label(new Rect(30, 100, 400, 30), "Level: " + GameState.Instance.getLevel());
        GUI.Label(new Rect(30, 130, 400, 30), "Money: " + GameState.Instance.getExp().ToString());
    }
}
