﻿using UnityEngine;
using System.Collections;

public class BulletDet : MonoBehaviour {
	public float flyspeed;
	public Transform target;
	public int damage;

	void Update(){
		transform.position = Vector3. MoveTowards(transform.position, target.position, flyspeed*Time.deltaTime);
	}
	public void SetTarget(Transform target){
				this.target = target;
		}
	void OnCollisionEnter (Collision other){
		if (other.gameObject.tag == "enemy"){
			other.gameObject.GetComponent<Enemy>().life -= damage;
			Destroy(gameObject);
		}
		if (other.gameObject.tag == "background"){
			Destroy(gameObject);
		}
	}}
