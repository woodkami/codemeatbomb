﻿using UnityEngine;
using System.Collections;

public class EnemyGoal : MonoBehaviour 
{
    void OnTriggerEnter(Collider c)
    {
        Destroy(c.gameObject);
    }
}
