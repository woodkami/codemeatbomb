﻿using UnityEngine;
using System.Collections;

public class EMP : MonoBehaviour {
	public float flyspeed;
	public Vector3 target;
	public Rigidbody EmpwavePrefab;
	
	void Update(){
		transform.position = Vector3. MoveTowards(transform.position, target, flyspeed*Time.deltaTime);
	}
	public void SetTarget(Vector3 target){
		this.target = target;
	}
	void OnCollisionEnter (Collision other){
		if (other.gameObject.tag == "background"){
			Rigidbody empInstance;
			empInstance = Instantiate (EmpwavePrefab,transform.position,transform.rotation) as Rigidbody;
		}
	}}
